# vue-luckyreel
The great mine field component for Vue.js applications.

![vue-luckyreel](https://user-images.githubusercontent.com/629338/110730057-c349e700-825a-11eb-942a-d3c3b82cb6eb.png)

Demo on [GitHub Pages](https://seokky.github.io/vue-luckyreel/)

## Features
- custom configurations
- locked, unlock, load data, clear reset, disabled, undisabled
- full scss style framework

## Attention!

Styles that component have are written just for demo. But that styles are override-friendly, so you can write any styles you want.

## Usage

```
  npm i --save vue-luckyreel
```
or with yarn
```
  yarn add vue-luckyreel
```

Then in any component:

```
import MineField from 'vue-luckyreel';
// The name can be different depending on your desire
```

```
import {ReelYah} from "vue-luckyreel"


<div class="input-wrapper">
  <reel-yah
        ref="pickmatrix"
             :lvl1w="8"
             :lvl1h="5"
             :box-length="40 + 10"
             :max-picked="max_pick"
             @update_list="setlist"
  />
</div>
```



**Attention**: you should use .. in order to rule  ...

```
<style lang="scss">
@import "@node_modules/vue-luckyreel/src/presents/base";
//@import "../presents/base";
.roulette_straight_rolex {
  .r-box {
    &:nth-child(odd) {
      background: #5900ff;
    }

    &:nth-child(even) {
      background: #ff0066;
    }

    &:nth-child(1) {
      background: green !important;
    }
  }

}


<style>
```


**Attention**: you need to make setup of gsap animation plugin before hand it is recommended to use the version 3.3.4 greensock.
```

//plugins
import { TimelineLite, TimelineMax, TweenLite, TweenMax } from "gsap/all"
import Draggable from "gsap/Draggable"
import MotionPathPlugin from "gsap/MotionPathPlugin"
import MorphSVGPlugin from "gsap/MorphSVGPlugin"
import ScrollToPlugin from "gsap/ScrollToPlugin"
import ScrollTrigger from "gsap/ScrollTrigger"
import CSSPlugin from "gsap/CSSPlugin"
import DrawSVGPlugin from "gsap/DrawSVGPlugin"
import ScrambleTextPlugin from "gsap/ScrambleTextPlugin"
import Physics2DPlugin from "gsap/Physics2DPlugin"


import gsap from "gsap"
// don't forget to register plugins
gsap.registerPlugin(
    TimelineLite, TimelineMax, TweenMax,
    DrawSVGPlugin, ScrollTrigger, Draggable, MotionPathPlugin,
    ScrollToPlugin, Physics2DPlugin, MorphSVGPlugin, ScrambleTextPlugin, CSSPlugin)
// TweenMax, TweenLite, TimelineMax, TimelineLite,
const module = {
  install: (Vue, opts) => {
    //    that.$GSLite = new TimelineLite ();
    Vue.prototype.$gsapTimeLine = function (data) {
      return gsap.timeline(data)
    }
    Vue.prototype.GSAP = function (data) {
      return gsap
    }
  }
}

Vue.use(module)

```


## Props

- **box_w** (size of the box)
  - type: Number
  - default: 0

- **roulette_max_num** (number of total boxes)
  - type: Number
  - default: 0

- **reel_roll_state_rolling** (event that exceed the max picked counts)
  - type: Event
  
- **reel_roll_complete** (event error from locked)
  - type: Event

- **reel_debug** (event debug information)
  - type: Event
  
## ToDo

    Note: in Minesweeper mode, you need to click at least 7 times with 1 bomb, 5 times with 3 bombs, 3 times with 5
        bombs or once with 24 bombs in order to receive wager.
    
         The player can login the bigbang game platform and make deposit of the token BBGC, HT, ZB, OKB, ETH, GTS and the
            supported tokens. The player can obtain the tokens from the exchange platforms. HOTBIT.io, FCOIN.com, EXX.com,
            and ZBG.com.
