const BASE_REF_NAME = 'vue-codepin';
const CELL_REGEXP = '^\\d{1}$';
const DEFAULT_INPUT_TYPE = 'tel';
const SECURE_INPUT_TYPE = 'password';
const INPUT_REFER = 'referrer';
export { BASE_REF_NAME, CELL_REGEXP, DEFAULT_INPUT_TYPE, SECURE_INPUT_TYPE, INPUT_REFER, };
//# sourceMappingURL=constants.js.map