export default {
  props: {
    box_w: {
      type: Number,
      required: false,
      default: 50
    },
    roulette_max_num: {
      type: Number,
      required: false,
      default: 37
    },
    debug_ds: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  data() {
    return {
      repeat_full: 10,
      item_full: 0,
      items: [],
      rolling: false,
      ready_roll_again: true,
      out_display: 0,
      rx_x: 0,
      run_tl: 0,
      target_n: 0,
      styleObject: {
        scroller: {},
        timerDiv: {},
        buttonStatus: "active"
      }
    }
  },
  methods: {

  }
}
