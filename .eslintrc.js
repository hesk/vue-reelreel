module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/airbnb',
    '@vue/typescript',
    "eslint:recommended",
    "plugin:vue/essential"
  ],
  rules: {
    "no-undef": "warn",
    "no-console": "warn",
    "indent": [
      "error",
      2,
      {
        "SwitchCase": 1
      }
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "never"
    ],

    "space-before-function-paren": [
      "off"
    ],
    "object-property-newline": [
      "error"
    ]
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: [
    "vue"
  ],
  overrides: [
    {
      files: [
        '**/__tests__/*.{t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
